;
/**
 * Mobile Menu Ajax
 *
 * A jquery/Zepto Plugin for building menu for mobile with Ajax
 *
 */

(function($) {
  'use strict';
  var pluginName = 'MobileMenuAjax',
    defaults = {
      elt: '',
      toolbar: '<div class="toolbar"></div>',
      toolbarHeader: '<span class="header"></span>',
      toolbarBreadcrumb: '<span class="breadcrumb"></span>',
      iconPrev: '<a class="btn btn_previous"><i class="icon-chevron-left"></i></a>',
      iconList: '<a class="btn btn_list"><i class="icon-list"></i></a>',
      cssBtnPrev: 'btn btn_previous',
      cssBtnList: 'btn btn_list',
      cssAjaxLoad: 'nav_loading',
      dataUrlAjax: 'data-rel',
      current: 'ul.nav',
      timeOpen: 300,
      timeClose: 300,
      onInit: function() {},
      debug: false
    };
  var nav = {}
  /**
   * Plugin Constructor. This function build the basic object for the plugin
   * @param (object) element - The jQuery or Zepto DOM element
   * @param (object) options - A list of options for the plugin
   */
  $[pluginName] = function(element, options) {

    this.options = $.extend({}, defaults, options)
    this._defaults = defaults
    this._name = pluginName
    this.debug = this.options.debug
    this.element = element
    this.$element = $(element)
    this.elementHTML = this.$element.html()
    //this.id = this.$element[0].id
    //this.idSelector = '#' + this.id
    this.content = this.$element.find('ul.nav')

    this.nav = {
      current: this.$element.find('ul.nav'),
      histo: []
    }

    // define media type
    this.clickTapEvent = 'onTap' in window ? 'tap' : 'click'

    this.header = $(this.options.toolbarHeader);
    this.breadcrumb = $(this.options.toolbarBreadcrumb);
    this.iconPrev = $(this.options.iconPrev);
    this.iconList = $(this.options.iconList);

    this.toolbar = $(this.options.toolbar).append(this.iconPrev).append(this.header).append(this.breadcrumb)

    // Launch the tab plugins
    this.initialize()
  };

  // Plugin prototype
  /**
   * The plugin prototype that list all availables methods.
   * @type {Object}
   */
  $[pluginName].prototype = {
    /**
     * initialize the plugin. First it gets all the elements then build the component. When all is done it fires the init option callback
     *
     */
    initialize: function() {
      this.debug && console.log('debug')
      //callback onInit
      typeof this.options.onInit === 'function' && this.options.onInit()
      this.build()
    },
    /**
     * Build the menu
     * @return {object} nav - The jQuery/Zepto DOM Element containing the navigation
     */
    build: function() {
      // Nav toggle
      this.$element.find('.nav').css({
        height: 'auto'
      })
      // Sliding nav
      this.bindSlide()
    },
    loadChild: function(controler, callback) {

      this.debug && console.log("fn:loadChild")
      var nav = this.nav
      var thisOps = this.options
      var thisPlg = this
      var childURL = controler.attr(thisOps.dataUrlAjax)
      if (controler.attr('data-init') != 'true' && childURL != '') {
        controler.attr('data-init', 'true')
        controler.addClass(thisOps.cssAjaxLoad)
        $.ajax({
          url: childURL,
          type: 'json',
          success: function(resp) {
            $(resp.content).insertAfter(controler)
            $(resp.content).find('ul').each(function() {
              $(this).css({
                right: '-' + thisPlg.$element.width() + 'px'
              })
            })
            controler.removeClass(thisOps.cssAjaxLoad)
            if (typeof callback === 'function') callback(controler)
          }
        })
      }

    },
    /**
     * update menu
     */
    updateNav: function() {
      this.debug && console.log("fn:updateNav")

      var nav = this.nav
      var thisOps = this.options
      var thisPlg = this
      var $i = 0

      nav.histo = []

      nav.previous = $(nav.current.parents('ul')[0])

      if (!this.content.parent().find(this.toolbar).length) {
        this.toolbar.insertBefore(this.content)
      } else {
        this.toolbar.show()
      }
      this.toolbar.find('span').empty()
      this.content.css({
        height: nav.current.height() + 'px'
      })
      $(this.content.find('a.active')).each(function() {
        nav.histo.push($(this))
      })
      if (nav.histo.length > 1) {
        if (!nav.listCurrent) {
          nav.listCurrent = this.iconList.appendTo(this.toolbar)
        } else {
          nav.listCurrent = this.iconList
        }
        this.iconList.show().attr('href', nav.histo.slice(-1)[0].attr('href'))
      } else {
        this.iconList.hide()
      }

      $(nav.histo).each(function() {
        $i++
        var origin = $(this)
        var step = ($i != nav.histo.length) ? origin[0].cloneNode(true) : $('<span class="current">' + origin.text() + '</span>')

        $(step).appendTo(thisPlg.breadcrumb)
        thisPlg.header.text($(step).text())
      })

      this.iconPrev.unbind(this.clickTapEvent)
        .bind(this.clickTapEvent, function(e) {
          e.stopPropagation()
          thisPlg.scrollTop()
          if (!nav.current.hasClass('nav')) {
            nav.histo = nav.histo.slice(0, -1)
            nav.current.removeClass('active')
            $(nav.current[0]).animate({
              "translate3d": '100%,0,0'
            }, thisOps.timeClose, function() {
              nav.current = nav.previous
              nav.current.addClass('active').find('a.active').removeClass('active')
              thisPlg.content.css({
                height: nav.current.height() + 'px'
              })
              if (nav.current.hasClass('nav')) {
                thisPlg.toolbar.hide()
                thisPlg.content.css({
                  height: 'auto'
                })
              } else {
                thisPlg.updateNav()
              }
            })
          }
        })
    },
    /**
     * open and slide next nav
     * @param {dom} [controler] [content menu slide]
     */
    slideNav: function(controler) {
      this.debug && console.log("fn:slideNav")

      var nav = this.nav
      var thisPlg = this
      var thisOps = this.options
      var container = controler.next('ul')

      nav.current.find('a').unbind(this.clickTapEvent).bind(this.clickTapEvent, function(e) {
        e.preventDefault()
      })
      if (container.length) {
        nav.current = container
        nav.previous = $(controler.parents('ul')[0])
        controler.addClass('active')
        this.$element.find('.nav ul').each(function() {
          if ($(this) != nav.current) $(this).removeClass('active')
        })

        nav.current.addClass('active').css({
          right: '-' + this.$element.width() + 'px'
        })
        $(nav.current[0]).animate({
          "translate3d": '-' + this.$element.width() + 'px,0,0',
          "margin-left": "-1px"
        }, thisOps.timeOpen, function() {
          thisPlg.bindSlide()
        })
        thisPlg.updateNav()
        thisPlg.scrollTop()
      }
    },
    /**
     * init link event & data-init if true for Ajax
     *
     */
    bindSlide: function() {
      this.debug && console.log("fn:bindSlide")
      var nav = this.nav
      var thisPlg = this
      var thisOps = this.options

      this.$element.find('> ul a').each(function(i) {
        var controler = $(this)
        controler
          .unbind(thisPlg.clickTapEvent).bind(thisPlg.clickTapEvent, function(e) {
            if ( !! controler.attr(thisOps.dataUrlAjax) && controler.attr('data-init') != 'true') {
              e.preventDefault()
              thisPlg.loadChild(controler, function() {
                thisPlg.slideNav(controler)
                thisPlg.bindSlide()
              })
            } else {
              if (controler.next('ul').length) {
                e.preventDefault()
                thisPlg.slideNav(controler)
              }
            }
          })
      })
    },
    /**
     *
     */
    scrollTop: function() {
      window.scrollTo(0, this.$element.offset().top)
    },
    /**
     * destroy menu
     */
    destroy: function() {
      var nav = this.nav
      nav.histo = []
      this.toolbar.remove()
      //this.$element.insertAfter('#header')
      this.content
        .css({
          height: 'auto'
        })
        .find('ul').removeAttr('style')
      this.content.find('a').unbind('click').removeClass('active')
    }
  };

  // Building the plugin
  /**
   * The plugin component
   * @param  {object} options - list of all parameters for the jQuery/Zepto module
   * @return {object} - The jQuery/Zepto DOM element
   */
  $.fn[pluginName] = function(options) {
    return this.each(function() {
      if (!$(this).data(pluginName)) {
        if (options === 'destroy') {
          return;
        }
        $(this).data(pluginName, new $[pluginName](this, options))
      } else {
        var $plugin = $(this).data(pluginName)
        switch (options) {
          case 'destroy':
            $plugin.destroy();
            break;
          default :
            $plugin.build();
        }
      }
    })
  }
})(window.Zepto || window.jQuery);