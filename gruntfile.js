module.exports = function(grunt) {

  grunt.initConfig({
    project: {
      name: "mobilemenuajax"
    },
    uglify: {
      def: {
        files: {
          '<%= project.name %>.min.js': 'src/<%= project.name %>.js'
        }
      }
    },
    cssmin: {
      def: {
        files: {
          '<%= project.name %>.min.css': 'src/<%= project.name %>.css'
        }
      }
    },
    watch: {
      def: {
        files: ['src/*.js'],
        tasks: [
          'uglify', 'cssmin'
        ],
        options: {
          spawn: false,
          livereload: {
            port: 9000
          }
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-uglify');
  // grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-watch');
  //grunt.loadNpmTasks('grunt-contrib-stylus');
  grunt.loadNpmTasks('grunt-newer');

  grunt.registerTask('default', ['watch:def']);
  grunt.registerTask('js-task', ['uglify']);
  grunt.registerTask('css-task', ['cssmin']);
  //grunt.registerTask('css-task', ['stylus', 'cssmin']);
  // grunt.registerTask('stylus-task', ['stylus']);
};